package com.westline.unicom.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class DateUtil {

	/** 
	 * 获取12个月的月第一天 和 本月的最后一天
	 * @throws ParseException 
	 */
	public static List<String> get2daysForSql(String date) throws ParseException{
		List<String> list = new LinkedList<String>(); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		//list.add(sdf.format(sdf.parse(date)));
		int year = Integer.parseInt(date.split("-")[0]);
		int month = Integer.parseInt(date.split("-")[1])-1;
		int day = Integer.parseInt(date.split("-")[2]);
		cal.setTime(sdf.parse(date));
		cal.set(Calendar.YEAR,   year); 
		cal.set(Calendar.MONTH,   month); 
		cal.set(Calendar.DATE, cal.getActualMinimum(cal.DAY_OF_MONTH));
		cal.add(Calendar.MONTH, -11);
		list.add(sdf.format(cal.getTime()));
		cal.set(year, month, day);
		cal.set(Calendar.YEAR,   year); 
		cal.set(Calendar.MONTH,   month); 
		cal.set(Calendar.DATE, cal.getActualMaximum(cal.DAY_OF_MONTH));
		list.add(sdf.format(cal.getTime()));
		return list; 
	}
	public static String getDblStr(String dbl) {
		if (dbl!=null && dbl.length()>0){
			if (dbl.indexOf(".")!=-1){
				dbl=dbl.trim();
				if (dbl.substring(0,1).equals("."))
					dbl="0"+dbl;
				
				
			}
		}
		return dbl;
	}
	/**
	 * 获取某月第一天
	 */
	public static String getFirstDayOfMonth(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		int year = Integer.parseInt(date.split("-")[0]);
		int month = Integer.parseInt(date.split("-")[1])-1;
		int day = Integer.parseInt(date.split("-")[2]);
		cal.set(Calendar.YEAR,   year); 
		cal.set(Calendar.MONTH,   month); 
		cal.set(Calendar.DATE, cal.getActualMinimum(cal.DAY_OF_MONTH));
		return sdf.format(cal.getTime());
	}
	/**
	 * 获取某月最后一天
	 */
	public static String getLastDayOfMonth(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		int year = Integer.parseInt(date.split("-")[0]);
		int month = Integer.parseInt(date.split("-")[1])-1;
		int day = Integer.parseInt(date.split("-")[2]);
		cal.set(Calendar.YEAR,   year); 
		cal.set(Calendar.MONTH,   month); 
		cal.set(Calendar.DATE, cal.getActualMaximum(cal.DAY_OF_MONTH));
		return sdf.format(cal.getTime());
	}
	
	/**
	 * 获取某年第一天
	 */
	public static String getFirstDayOfYear(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		int year = Integer.parseInt(date.split("-")[0]);
		Calendar calendar = Calendar.getInstance();   
	    calendar.clear();   
	    calendar.set(Calendar.YEAR, year);   
		return sdf.format(calendar.getTime());
	}
	/**
	 * 获取某年最后一天
	 */
	public static String getLastDayOfYear(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		int year = Integer.parseInt(date.split("-")[0]);
		Calendar calendar = Calendar.getInstance();   
	    calendar.clear();   
	    calendar.set(Calendar.YEAR, year);   
	    calendar.roll(Calendar.DAY_OF_YEAR, -1);   
		return sdf.format(calendar.getTime());
	}
	/**
	 * 获取昨天
	 */
	public static String getToday(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		return sdf.format(cal.getTime());
	}
	/**
	 * 获取昨天
	 */
	public static String getYesterDay(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(cal.DATE, -1);
		return sdf.format(cal.getTime());
	}
	/**
	 * 获取下一周的所有日期
	 */
	public static List<String>  getNextWeek(){
		List<String> list = new LinkedList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(cal.DATE, 7);
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		list.add(sdf.format(cal.getTime()));
		return list;
	}
	/**
	 * 获取上一周的所有日期
	 */
	public static  List<String>  getPreWeek(){
		List<String> list = new LinkedList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(cal.DATE, -7);
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		list.add(sdf.format(cal.getTime()));
		return list;
	}
	/**
	 * 获取本周的所有日期
	 */
	public static  List<String>  getCurrentWeek(){
		List<String> list = new LinkedList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		list.add(sdf.format(cal.getTime()));
		return list;
	}
	/**
	 * 获取最近7天
	 * @param args
	 * @throws ParseException 
	 * @throws ParseException 
	 * @throws Exception
	 */
	public static  List<String>  getLastSeventDay(String date) throws ParseException {
		List<String> list = new LinkedList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		list.add(sdf.format(sdf.parse(date)));
		int year = Integer.parseInt(date.split("-")[0]);
		int month = Integer.parseInt(date.split("-")[1])-1;
		int day = Integer.parseInt(date.split("-")[2]);
		cal.set(year, month, day);
		for(int i=-6;i<0;i++){
			cal.add(cal.DATE, -1);
			list.add(sdf.format(cal.getTime()));
		}
		Collections.reverse(list);
		return list;
	}
	
	/**
	 * 周天开始 获取一周内的具体天数
	 * @param monthStr
	 * @return
	 */
	public static  List<String>  getWeekDays(String date) throws ParseException{
		List<String> list = new LinkedList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		//list.add(sdf.format(sdf.parse(date)));
		int year = Integer.parseInt(date.split("-")[0]);
		int month = Integer.parseInt(date.split("-")[1])-1;
		int day = Integer.parseInt(date.split("-")[2]);
		cal.setTime(sdf.parse(date));
		cal.set(year, month, day);
		cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
		list.add(sdf.format(cal.getTime()));
		cal.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
		list.add(sdf.format(cal.getTime()));
		return list;
	}
	
	
	
	public static List<String> getDaysByMonth(String monthStr){
		List<String> list = new LinkedList<String>();
		int year = Integer.parseInt(monthStr.split("-")[0]);
		int month = Integer.parseInt(monthStr.split("-")[1])-1;
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, 1);
		int totalDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		for(int i=1;i<=totalDays;i++){
			list.add(monthStr+"-"+(i<10?"0"+i:i));
		}
		return list;
	}

	public static List<String> getLast12Months(String monthStr){
		List<String> list = new LinkedList<String>();
		int year = Integer.parseInt(monthStr.split("-")[0]);
		int month = Integer.parseInt(monthStr.split("-")[1]);
		for(int i=0;i<12;i++){
			if(month>0){
				list.add(year+"-"+(month<10?"0"+month:month));
				month--;
			}else{
				list.add((year-1)+"-"+((month+12)<10?"0"+(month+12):(month+12)));
				month--;
			}
		}
		Collections.reverse(list);
		return list;
	}
	public static String nowDateBeforDay(int i)
	  {
	    java.util.Calendar cal = java.util.Calendar.getInstance();
	    cal.add(java.util.Calendar.DATE, i);
	    Date tomnow = new Date(cal.getTimeInMillis());
	    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	    String Send_time = df.format(tomnow);
	    return Send_time;
	  }

	   public static String nowDateFormat()
	   {
	     Date tomnow = new Date();
	     SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	     String time = df.format(tomnow);
	     return time;
	   }
	public static void main(String args[]) throws ParseException{
//		System.out.println(getFirstDayOfYear("2012-01-06"));
//		System.out.println(getLastDayOfYear("2012-01-06"));
		for(String s:get2daysForSql("2012-02-26")){
			System.out.println(s);
		}
//		System.out.println("===================");
//		for(String s:getWeekDays("2012-02-05")){
//			System.out.println(s);
//		}
//		System.out.println("===================");
//		for(String s:getDaysByMonth("2012-08")){
//			System.out.println(s);
//		}
	}
	/*
	 * 后付费实时账单
	 */
	public static String[] getGfeePostPay(String mess){
		if (mess==null || mess.length()==0)
		return null;
		if (mess.indexOf("xception")!=-1 || mess.indexOf("fail")!=-1)
		return null;
		String yuee="",qianfei="",huafei="";
		yuee=mess.substring(mess.length()-10,mess.length());
		yuee=yuee.trim();
		if (yuee.equals(""))
			yuee="0";
		qianfei=mess.substring(mess.length()-80,mess.length()-70);
		qianfei=qianfei.trim();
		if (qianfei.equals(""))
			qianfei="0";
		huafei=mess.substring(mess.length()-90,mess.length()-80).trim();
		if (huafei.equals(""))
			huafei="0";
		String[] returns=new String[3];
		returns[0]=yuee;
		returns[1]=huafei;
		returns[2]=qianfei;
		return returns;
	}
	public static String[] getGfeePrePay(String mess){
		if (mess==null || mess.length()==0)
		return null;
		if (mess.indexOf("xception")!=-1 || mess.indexOf("fail")!=-1)
		return null;
		String yuee="",qianfei="",huafei="";
		yuee=mess.substring(mess.length()-10,mess.length());
		yuee=yuee.trim();
		if (yuee.equals(""))
			yuee="0";
		qianfei=mess.substring(mess.length()-80,mess.length()-70);
		qianfei=qianfei.trim();
		if (qianfei.equals(""))
			qianfei="0";
		huafei=mess.substring(mess.length()-90,mess.length()-80).trim();
		if (huafei.equals(""))
			huafei="0";
		String[] returns=new String[3];
		returns[0]=yuee;
		returns[1]=huafei;
		returns[2]=qianfei;
		return returns;
	}
	/**
	 * 检测号码是后付费还是预付费
	 * 0失败
	 * 1后付费
	 * 2预付费
	 * @param mess
	 * @return
	 */
	public static int getGuserProp(String mess){
		int returns=0;
		if (mess==null || mess.length()==0)
		return returns;
		if (mess.indexOf("xception")!=-1)
			return returns;
		try{
			returns=Integer.parseInt(mess.substring(mess.length()-47,mess.length()-46));
		}catch(Exception e){
			e.printStackTrace();
			returns=0;
		}
		return returns;
	}
	/**
	 * 历史账单
	 * 增值业务费
	 * --代收费
	 * --短消息费
	 * --手机邮箱包月费
	 * 月固定费
	 * --GPRSWAP月租费
	 * --来电显示费
	 * --3G包月费
	 * 消费合计
	 * 抵扣合计
	 * 实际应缴合计
	 * @param mess
	 * @return
	 */
	public static String[] getHistoryBill(String mess){
		String zhenzhi="",daishou="",duanxiaoxi="",shoujiyouxiang="",yueguding="",gprswap="",laidian="";
		String threeG="",xiaofeiheji="",dikouheji="",yingjiaoheji="";
		String returns[]=new String[11];
		if (mess==null || mess.length()==0)
		return null;
		if (mess.indexOf("xception")!=-1)
		return null;
		try{
			int begin=0;
			returns[0]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[1]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[2]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[3]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[4]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[5]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[6]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[7]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[8]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[9]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
			returns[10]=mess.substring(mess.indexOf(":",begin)+1,mess.indexOf(";",begin));
			begin=mess.indexOf(";",begin)+1;
		}catch(Exception e){
			return null;
		}
		return returns;
	}

	public static List<Map> getHistoryBill2(String mess){
		System.out.println("mess:"+mess);
		List<Map> messList=new ArrayList<Map>();
		if (mess==null || mess.length()==0)
		return null;
		if (mess.indexOf("xception")!=-1)
		return null;
		try{
			mess=mess.substring(mess.indexOf(" 100000")+7,mess.length()-1);
			System.out.println("mess:"+mess);
			String messArr[]=mess.split(";");
			for (int i=0;i<messArr.length;i++){
				String mes=messArr[i];
				System.out.println("mes:"+mes);
				if (mes.indexOf(":")!=-1){
				String[] mesArr=mes.split(":");
				Map mesM=new HashMap();
				mesM.put("name", mesArr[0]);
				mesM.put("cost", mesArr[1]);
				mesM.put("order", i+1);
				if (mesArr[0].indexOf("--")!=-1)
					mesM.put("level", 2);
				else
					mesM.put("level", 1);
				messList.add(mesM);
				}
			}
		}catch(Exception e){
			return null;
		}
		return messList;
	}
}    

