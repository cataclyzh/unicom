package com.westline.unicom.service.impl;

import java.util.List;
import java.util.Map;

import com.westline.unicom.dao.BillDao;
import com.westline.unicom.service.BillService;

public class BillServiceImpl implements BillService {
	public BillDao billDao;

	public boolean isHaveBingNum(String sn){
		return billDao.isHaveBingNum(sn);
	}
	public Object getDefaultBingNum(String sn){
		return billDao.getDefaultBingNum(sn);
	}
	public Object getDefaultBingNum(String sn,String telnum){
		return billDao.getDefaultBingNum(sn,telnum);
	}
	public Object getBingNumById(int bindId){
		return billDao.getBingNumById(bindId);
	}
	/**
	 * 刷新绑定号码的实时信息
	 * @return
	 */
	public void reFlashNumInfo(String balance,String realtimecharge,String arrearage,String bindnum,String sn){
		billDao.reFlashNumInfo(balance, realtimecharge, arrearage, bindnum,sn);
		
	}
	public void insertNumInfo(String sn,String bindnumber,int isdefault,int carrieroperator,int ispostpay,String balance,String realtimecharge,String arrearage){
		billDao.insertNumInfo(sn, bindnumber, isdefault, carrieroperator, ispostpay, balance, realtimecharge, arrearage);
	}
	public void updateNumInfo(String sn,int bindId,String bindnumber,int isdefault,int carrieroperator,int ispostpay,String balance,String realtimecharge,String arrearage){
		billDao.updateNumInfo(sn, bindId, bindnumber, isdefault,carrieroperator,ispostpay,balance,realtimecharge,arrearage);
	}
	public void removeNumInfo(int bindId){
		billDao.removeNumInfo(bindId);
	}
	 public List getBingNumList(String sn){
		 return billDao.getBingNumList(sn);
		 
	 }

	public void insertBillInfo(int bindid,int year,int month,String addedservice,String generationcharge,String message,String mobilemail,String monthlyfixed,String gprswap,String callerdisplay,String threegmonth,String totalconsumption,String totaldeduction,String totalpay){
		billDao.insertBillInfo(bindid, year, month, addedservice, generationcharge, message, mobilemail, monthlyfixed, gprswap, callerdisplay, threegmonth, totalconsumption, totaldeduction, totalpay);
	}
	public void reFlashBillInfo(int bindid,int year,int month,String addedservice,String generationcharge,String message,String mobilemail,String monthlyfixed,String gprswap,String callerdisplay,String threegmonth,String totalconsumption,String totaldeduction,String totalpay){
		billDao.reFlashBillInfo(bindid, year, month, addedservice, generationcharge, message, mobilemail, monthlyfixed, gprswap, callerdisplay, threegmonth, totalconsumption, totaldeduction, totalpay);
	}
	public Object getBill(int bindid,int year,int month){
		return billDao.getBill(bindid, year, month);
	}

	public void insertBillHistoryInfo(int bindid,int year,int month,List<Map> billList){
		billDao.insertBillHistoryInfo(bindid, year, month, billList);
	}
	public void reFlashBillHistoryInfo(int bindid,int year,int month,List<Map> billList){
		billDao.reFlashBillHistoryInfo(bindid, year, month, billList);
	}
	public List getBillHistoryInfo(int bindid,int year,int month){
		return billDao.getBillHistoryInfo(bindid, year, month);
	}
	 
	 
	 
	 
	 
	public BillDao getBillDao() {
		return billDao;
	}

	public void setBillDao(BillDao billDao) {
		this.billDao = billDao;
	}
	
	
}
