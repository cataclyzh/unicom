package com.westline.unicom.service;

import java.util.List;
import java.util.Map;

public interface BillService {

	public boolean isHaveBingNum(String sn);
	public Object getDefaultBingNum(String sn);
	public Object getDefaultBingNum(String sn,String telnum);
	public Object getBingNumById(int bindId);
	/**
	 * 刷新绑定号码的实时信息
	 * @return
	 */
	public void reFlashNumInfo(String balance,String realtimecharge,String arrearage,String bindnum,String sn);
	public void insertNumInfo(String sn,String bindnumber,int isdefault,int carrieroperator,int ispostpay,String balance,String realtimecharge,String arrearage);
	public void updateNumInfo(String sn,int bindId,String bindnumber,int isdefault,int carrieroperator,int ispostpay,String balance,String realtimecharge,String arrearage);
	public void removeNumInfo(int bindId);
	public List getBingNumList(String sn);
	public void insertBillInfo(int bindid,int year,int month,String addedservice,String generationcharge,String message,String mobilemail,String monthlyfixed,String gprswap,String callerdisplay,String threegmonth,String totalconsumption,String totaldeduction,String totalpay);
	public void reFlashBillInfo(int bindid,int year,int month,String addedservice,String generationcharge,String message,String mobilemail,String monthlyfixed,String gprswap,String callerdisplay,String threegmonth,String totalconsumption,String totaldeduction,String totalpay);
	public Object getBill(int bindid,int year,int month);
	public void insertBillHistoryInfo(int bindid,int year,int month,List<Map> billList);
	public void reFlashBillHistoryInfo(int bindid,int year,int month,List<Map> billList);
	public List getBillHistoryInfo(int bindid,int year,int month);
}
