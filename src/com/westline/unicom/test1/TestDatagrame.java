package com.westline.unicom.test1;

public class TestDatagrame {

	public static void main(String[] args) {
		// String mobile = args[0];
		int length = 0;
		String a0 = "10";
		length += a0.length();
		String a1 = null;
		length += 5;
		String a2 = formatString(String.valueOf(System.currentTimeMillis()), "0", 20);
		length += a2.length();
		String a3 = "1";
		length += a3.length();
		String a4 = formatString("102011201001", null, 12);
		length += a4.length();
		// String a5=formatString("13182982692",null,20);
		// gll
		// String a5 = formatString(mobile, null, 20);
		String a5 = formatString("13112345678", null, 20);
		length += a5.length();
		String a6 = "G";
		length += a6.length();
		String a7 = "A0000CHQ";
		length += a7.length();
		String a8 = "Z0000025";
		length += a8.length();
		String a9 = formatString("1", null, 5);
		length += a9.length();
		String a10 = "1";
		length += a10.length();
		String a11 = formatString("", null, 5);
		length += a11.length();

		String ym = "201411";
		length += ym.length();
		length += 1;
		a1 = formatString(String.valueOf(length), null, 5);

		StringBuffer dest = new StringBuffer();
		dest.append(a0);
		dest.append(a1);
		dest.append(a2);
		dest.append(a3);
		dest.append(a4);
		dest.append(a5);
		dest.append(a6);
		dest.append(a7);
		dest.append(a8);
		dest.append(a9);
		dest.append(a10);
		dest.append(a11);
		dest.append(ym);
		//dest.append("\n");
		char[] charts = new char[length];
		String str = dest.toString();
		
		System.out.println(length);
		System.out.print("##1:" + dest.toString() + "##");
	}

	private static String formatString(String src, String fill, int length) {
		if (src.length() > length) {
			return src;
		}
		StringBuffer dest = new StringBuffer(src);
		for (int i = 0; i < length - src.length(); i++) {
			if (fill != null) {
				dest.append(fill);
			} else {
				dest.append(" ");
			}
		}
		return dest.toString();
	}
}
