package com.westline.unicom.test;

import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
import java.io.PrintWriter;  
import java.net.ServerSocket;  
import java.net.Socket;  
  
public class testServer extends Thread {  
    private Socket client;  
  
    public testServer(Socket c) {  
        this.client = c;  
    }  
  
    public void run() {  
        try {  
            BufferedReader in = new BufferedReader(new InputStreamReader(  
                    client.getInputStream()));  
            PrintWriter out = new PrintWriter(client.getOutputStream());
            
            System.out.println("begin"); 
            String str;
            while((str = in.readLine())!=null){
            	System.out.println("link"); 
                str = in.readLine();  
                System.out.println(str); 
                String st[]=str.split(";");
                testUnicomSocket uniCom=null;
                if (st.length==2)
                 uniCom=new testUnicomSocket("130.34.3.48",10190,st[0],st[1]);
                else
                 uniCom=new testUnicomSocket("130.34.3.48",10190,st[0],st[1],st[2]);
                out.println(uniCom.linkUniCom());  
                out.flush(); 
            }
            client.close();  
        } catch (IOException ex) {  
        	ex.printStackTrace();
        } finally {  
        }  
    }  
  
    public static void main(String[] args) throws IOException {  
        ServerSocket server = new ServerSocket(10999);  
        while (true) {  
        	System.out.println("Listenning accept...");
        	ServerThread mc = new ServerThread(server.accept());  
            mc.start();  
        }  
    }  
}