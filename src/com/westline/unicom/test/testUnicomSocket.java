package com.westline.unicom.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class testUnicomSocket {
	public static String unicomHost;
	public int unicomHostPort;
	public static String tel;
	public static String con;
	public static String parm=null;
    public testUnicomSocket( String unicomhost,int unicomhostPort,String tele,String orders,String parme) {  
    	unicomHost= unicomhost;
    	unicomHostPort=unicomhostPort;
    	tel=tele;
    	con=orders;
    	parm=parme;
    }  

    public testUnicomSocket( String unicomhost,int unicomhostPort,String tele,String orders) {  
    	unicomHost= unicomhost;
    	unicomHostPort=unicomhostPort;
    	tel=tele;
    	con=orders;
    }  
  
    public  byte[] linkUniCom()
    {
    	
        String host = "130.34.3.48";
        int port = Integer.parseInt("10190");
        int length = 0;
        String a0 = "10";
        length += a0.length();
        String a1 = null;
        length += 5;
        String a2 = formatString(String.valueOf(System.currentTimeMillis()), "0", 20);
        length += a2.length();
        String a3 = "1";
        length += a3.length();
        String a4 = formatString(con, null, 12);
        length += a4.length();
        String a5 = formatString(tel, null, 20);
        length += a5.length();
        String a6 = "G";
        length += a6.length();
        String a7 = "A0000CHQ";
        length += a7.length();
        String a8 = "Z0000025";
        length += a8.length();
        String a9 = formatString("1", null, 5);
        length += a9.length();
        String a10 = "1";
        length += a10.length();
        String a11 = formatString("", null, 5);
        length += a11.length();
        length += 1;
        a1 = formatString(String.valueOf(length), null, 5);

        
        StringBuffer dest = new StringBuffer();
        dest.append(a0);
        dest.append(a1);
        dest.append(a2);
        dest.append(a3);
        dest.append(a4);
        dest.append(a5);
        dest.append(a6);
        dest.append(a7);
        dest.append(a8);
        dest.append(a9);
        dest.append(a10);
        dest.append(a11);
        if (parm!=null){
            length += parm.length();
            length += 1;
            dest.append(parm);
        }
        dest.append("\n");

    	tel=null;
    	con=null;
    	parm=null;
        char[] charts = new char[length];
        String str = dest.toString();
        str.getChars(0, str.length(), charts, 0);
        byte[] bytes = transCharToByte(charts);
        String returns = "";
        byte bits[] = new byte[1024];
        try
        {
            System.out.println("send to:" + unicomHost + ":" + port);
            Socket socket = new Socket(unicomHost, port);
            System.out.println("is connected " + socket.isConnected());
            
            socket.setSoTimeout(10000);
            InputStream input = socket.getInputStream();
            OutputStream out = socket.getOutputStream();
            
            //InputStreamReader inSR = new InputStreamReader(input,"");  
           // BufferedReader br = new BufferedReader(inSR);  
            out.write(bytes);
            out.flush();
            String strs = "";  
            //returns =  br.readLine().trim();  
                
            
           
            input.read(bits);
            char[] chars = transByteToChar(bits);
            System.out.println("chars is:");
            System.out.println(chars);
            System.out.println("GB2312 String is:");
            String resultStr = new String(bits, "gbk");
            System.out.println(resultStr);
            
            
           
        }
        catch (UnknownHostException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally{}
        
        return bits;
        
    }
    
    private static String formatString(String src, String fill, int length)
    {
        if (src.length() > length)
        {
            return src;
        }
        StringBuffer dest = new StringBuffer(src);
        for (int i = 0; i < length - src.length(); i++)
        {
            if (fill != null)
            {
                dest.append(fill);
            }
            else
            {
                dest.append(" ");
            }
        }
        return dest.toString();
    }
    
    private static byte[] transCharToByte(char[] charts)
    {
        byte bytes[] = new byte[charts.length];
        for (int i = 0; i < charts.length; i++)
        {
            bytes[i] = (byte)charts[i];
        }
        return bytes;
    }
    
    private static char[] transByteToChar(byte[] bytes)
    {
        char chars[] = new char[bytes.length];
        for (int i = 0; i < bytes.length; i++)
        {
            // System.out.print(bytes[i]+",");
            chars[i] = (char)bytes[i];
        }
        return chars;
    }
   
    private static char byteToChar(byte[] b)
    {
        char c = (char)(((b[0] & 0xFF) << 8) | (b[1] & 0xFF));
        return c;
    }
}
