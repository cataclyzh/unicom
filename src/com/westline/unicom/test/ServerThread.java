package com.westline.unicom.test;


import java.io.*;
import java.net.*;


public class ServerThread extends Thread{
	Socket sock;
    public ServerThread(Socket s)
    {
        sock =s ;
    }
    public void run()
    {
        try{
            InputStream in = (InputStream) sock.getInputStream();
            DataInputStream din = new DataInputStream(in);
            String str = din.readLine();
            System.out.println(str);
           // in.close();
            String st[]=str.split(";");
            testUnicomSocket uniCom=null;
            if (st.length==2)
            	uniCom=new testUnicomSocket("130.34.3.48",10190,st[0],st[1]);
            else
            	uniCom=new testUnicomSocket("130.34.3.48",10190,st[0],st[1],st[2]);
            byte[] returns=uniCom.linkUniCom();
            OutputStream out = sock.getOutputStream();
            DataOutputStream dos = new DataOutputStream(out);
            dos.write(returns);
            //dos.writeUTF(returns);
            sock.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

}
