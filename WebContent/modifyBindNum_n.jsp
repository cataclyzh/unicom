﻿<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script language="JavaScript" type="text/javascript">  

//登录按钮
		fEventListen(oLo, 'mouseover', function(){
			fCls(oLo, 'btn-login-hover', 'add');
		});
		fEventListen(oLo, 'mouseout', function(){
			oLo.className = 'btn btn-main btn-login';
		});
		fEventListen(oLo, 'mousedown', function(){
			fCls(oLo, 'btn-login-active', 'add');
		});
//|------------------------------------------------------------------------------------   
//|   
//| 说明：JS弹出全屏遮盖的对话框(弹出层后面有遮盖效果，兼容主流浏览器)   
//|       实现了在最大化、拖动改变窗口大小和拖动滚动条时都可居中显示。   
//|   
//| 注意：主要使用EV_modeAlert和EV_closeAlert这两个函数；   
//|       (EV_modeAlert-弹出对话框，EV_closeAlert-关闭对话框)；   
//|       注意：使用时，请在body标签内(不要在其它元素内)写一div，   
//|       再给这div赋一id属性，如：id="myMsgBox"，   
//|       然后就可以调用EV_modeAlert('myMsgBox')来显示了。   
//|       还有，请给你这div设置css：display:none让它在开始时不显示  
//|------------------------------------------------------------------------------------   
//|   
//用来记录要显示的DIV的ID值   
var EV_MsgBox_ID=""; //重要     
//弹出对话窗口(msgID-要显示的div的id)   
function EV_modeAlert(msgID){  
    //创建大大的背景框   
    var bgObj=document.createElement("div");  
    bgObj.setAttribute('id','EV_bgModeAlertDiv');  
    document.body.appendChild(bgObj);  
    //背景框满窗口显示   
    EV_Show_bgDiv();  
   //把要显示的div居中显示   
    EV_MsgBox_ID=msgID;  
    EV_Show_msgDiv();  
}  
  
//关闭对话窗口   
function EV_closeAlert(){  
    var msgObj=document.getElementById(EV_MsgBox_ID);  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    msgObj.style.display="none";  
    document.body.removeChild(bgObj);  
    EV_MsgBox_ID="";  
}  
  
//窗口大小改变时更正显示大小和位置   
window.onresize=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//窗口滚动条拖动时更正显示大小和位置   
window.onscroll=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//把要显示的div居中显示   
function EV_Show_msgDiv(){  
    var msgObj   = document.getElementById(EV_MsgBox_ID);  
   msgObj.style.display  = "block";  
    var msgWidth = msgObj.scrollWidth;  
    var msgHeight= msgObj.scrollHeight;  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var msgTop=bgTop+Math.round((bgHeight-msgHeight));  
    var msgLeft=bgLeft+Math.round((bgWidth-msgWidth)/2);  
    msgObj.style.position = "absolute";  
    msgObj.style.top      = msgTop+"px";  
    msgObj.style.left     = msgLeft+"px";  
    msgObj.style.zIndex   = "10001";  
      
}  
//背景框满窗口显示   
function EV_Show_bgDiv(){  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    bgObj.style.position   = "absolute";  
    bgObj.style.top        = bgTop+"px";  
   bgObj.style.left       = bgLeft+"px";  
    bgObj.style.width      = bgWidth + "px";  
    bgObj.style.height     = bgHeight + "px";  
    bgObj.style.zIndex     = "10000";  
    bgObj.style.background = "#777";  
    bgObj.style.filter     = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=60,finishOpacity=60);";  
    bgObj.style.opacity    = "0.6";  
}  
//网页被卷去的上高度   
function EV_myScrollTop(){  
    var n=window.pageYOffset   
    || document.documentElement.scrollTop   
    || document.body.scrollTop || 0;  
   return n;  
}  
//网页被卷去的左宽度   
function EV_myScrollLeft(){  
    var n=window.pageXOffset   
    || document.documentElement.scrollLeft   
    || document.body.scrollLeft || 0;  
    return n;  
}  
//网页可见区域宽   
function EV_myClientWidth(){  
    var n=document.documentElement.clientWidth   
    || document.body.clientWidth || 0;  
    return n;  
}  
//网页可见区域高   
function EV_myClientHeight(){  
    var n=document.documentElement.clientHeight   
    || document.body.clientHeight || 0;  
    return n;  
}  

var isDefault=0;
<s:if test="isDefault==1">
isDefault=1;
</s:if>
function changeDefault(obj){
	if (obj.checked==true)
		isDefault=1;
	else
		isDefault=0;
}
function bindNum(){	
			
			var telNum=$("#bindNum").val();
			if(telNum==''){
				alert("请输入您的联通号码！");
				$("#bindNum").focus();
				return;
			}
			var re = /^1(3[0-2]|5[56]|8[56]|4[5]|7[6])\d{8}$/;
			if (!re.test(telNum)){
				alert("请输入正确的联通号码！");
				$("#bindNum").focus();
				return;
			}
			var bingdId=<s:property value="bindId"/>;
			var datajson = {"telnum":telNum,"isDefault":isDefault,"bindId":bingdId};
			var url = 'ajaxUpdateBindNum.action';
			
			$.ajax({
	            type: "POST",
	            url: url,
	            dataType: "json",
	            data: datajson,
	            success: responseCreate,
	            error: function () {
	                alert("绑定失败！");
	            }
	        });
		}
function responseCreate(data, textStatus, jqXHR){			
	if(data.status=="ok"){
				<s:if test="isDefault==1">							
				location.href="index.action";
				</s:if>
				<s:else>
				location.href="bindNumManager.action";
				</s:else>
		}
		else {
			alert("该号码绑定失败！");
		}
}
</script>  

</head>
<body>
<div class=apptop>
<div class=app>
<div class="l1" onclick="location.href='bindNumManager.action'"><img src="images/appreturn.png" width="18" height="23" /></div>
<div class="l3">号码修改</div>
<div class="r1"  onclick="EV_modeAlert('envon')">
 <img src="images/appmore.png" name="i" width="27" height="28"   align="absmiddle" id=i  onmouseover="show(this)" onmouseout="hide()">
 
</div>

</div></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="140" height="35" align="center" class=" fz12">手机号码：</td>
    <td><input type="text" name="bindNum" id="bindNum" placeholder="输入联通电话号码" autofocus="autofocus" value="<s:property value="bindNumInfo.bindnumber" />"/></td>

  </tr>
  <tr>
    <td height="35" align="center" class=" fz12">设为默认：</td>
    <td> <s:if test="bindNumInfo.isdefault==1">
    	<input type="checkbox" name="isDefault" id="isDefault" checked="true" onclick="changeDefault(this)"/>
    </s:if>
    <s:else>
    	<input type="checkbox" name="isDefault" id="isDefault"  onclick="changeDefault(this)"/>
    </s:else></td>
  </tr>
  <tr>
    <td height="35" colspan="2" align="center" class=" fz12">	
    <input id="fm-login-submit" value="确定修改" class="fm-button fm-submit" type="submit" tabindex="4" name="submit-btn" onclick="bindNum()">
    
    </td>
  </tr>
</table>
<div id="envon" style="width:100%; background-color:#FFFFFF; border:1px solid #ccc; padding:10px; overflow:hidden; display:none;"> 
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';">
      <td align="center" class="fz12"  onclick="location.href='bindNumManager.action'" >绑定号码维护</td>
    </tr>
    <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center" class="fz12" onclick="location.href='changeBindNum.action'">切换号码</td>
      
    </tr>
     <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center" class="fz12" onclick="EV_closeAlert()">返 回</td>
    </tr>
  </table>
</div>
</body>
</html>
