<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />


</head>
<body>
<table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td height="50" bgcolor="#fffffd" style="height:20px;line-height:20px;text-align:left;font-size:14px; font-weight:300 ">非常抱歉，本应用只支持南京本地非4G联通号码查询，该号码存在以下原因:
</td></tr>
   <tr>
<td height="50" bgcolor="#fffffd" style="height:20px;line-height:20px;text-align:left;font-size:14px; font-weight:300 ">1、该号码已销号</td>
</tr>
   <tr>
<td height="50" bgcolor="#fffffd" style="height:20px;line-height:20px;text-align:left;font-size:14px; font-weight:300 ">2、该号码是外地号码</td>
</tr>
   <tr>
<td height="50" bgcolor="#fffffd" style="height:20px;line-height:20px;text-align:left;font-size:14px; font-weight:300 ">3、该号码是4G号码</td>
  </tr>
   <tr>
      <td  bgcolor="#d3d3bb" height="2"></td>
    </tr>
</table>


</body>
</html>