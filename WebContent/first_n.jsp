﻿<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript">  
 
//用来记录要显示的DIV的ID值   
var EV_MsgBox_ID=""; //重要     
//弹出对话窗口(msgID-要显示的div的id)   
function EV_modeAlert(msgID){  
    //创建大大的背景框   
    var bgObj=document.createElement("div");  
    bgObj.setAttribute('id','EV_bgModeAlertDiv');  
    document.body.appendChild(bgObj);  
    //背景框满窗口显示   
    EV_Show_bgDiv();  
   //把要显示的div居中显示   
    EV_MsgBox_ID=msgID;  
    EV_Show_msgDiv();  
}  
  
//关闭对话窗口   
function EV_closeAlert(){  
    var msgObj=document.getElementById(EV_MsgBox_ID);  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    msgObj.style.display="none";  
    document.body.removeChild(bgObj);  
    EV_MsgBox_ID="";  
}  
  
//窗口大小改变时更正显示大小和位置   
window.onresize=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//窗口滚动条拖动时更正显示大小和位置   
window.onscroll=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//把要显示的div居中显示   
function EV_Show_msgDiv(){  
    var msgObj   = document.getElementById(EV_MsgBox_ID);  
   msgObj.style.display  = "block";  
    var msgWidth = msgObj.scrollWidth;  
    var msgHeight= msgObj.scrollHeight;  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var msgTop=bgTop+Math.round((bgHeight-msgHeight));  
    var msgLeft=bgLeft+Math.round((bgWidth-msgWidth)/2);  
    msgObj.style.position = "absolute";  
    msgObj.style.top      = msgTop+"px";  
    msgObj.style.left     = msgLeft+"px";  
    msgObj.style.zIndex   = "10001";  
      
}  
//背景框满窗口显示   
function EV_Show_bgDiv(){  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    bgObj.style.position   = "absolute";  
    bgObj.style.top        = bgTop+"px";  
   bgObj.style.left       = bgLeft+"px";  
    bgObj.style.width      = bgWidth + "px";  
    bgObj.style.height     = bgHeight + "px";  
    bgObj.style.zIndex     = "10000";  
    bgObj.style.background = "#777";  
    bgObj.style.filter     = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=60,finishOpacity=60);";  
    bgObj.style.opacity    = "0.6";  
}  
//网页被卷去的上高度   
function EV_myScrollTop(){  
    var n=window.pageYOffset   
    || document.documentElement.scrollTop   
    || document.body.scrollTop || 0;  
   return n;  
}  
//网页被卷去的左宽度   
function EV_myScrollLeft(){  
    var n=window.pageXOffset   
    || document.documentElement.scrollLeft   
    || document.body.scrollLeft || 0;  
    return n;  
}  
//网页可见区域宽   
function EV_myClientWidth(){  
    var n=document.documentElement.clientWidth   
    || document.body.clientWidth || 0;  
    return n;  
}  
//网页可见区域高   
function EV_myClientHeight(){  
    var n=document.documentElement.clientHeight   
    || document.body.clientHeight || 0;  
    return n;  
}  
</script>  

</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="35" bgcolor="#d6d7d9" style="height:35px;line-height:35px;text-align:center;font-size:15px; font-weight:300 ">绑定号码:<s:property value="telnum" />
</td>
  </tr>
   <tr>
      <td  bgcolor="#d3d3bb" height="1"></td>
    </tr>
</table>
<div style="width:100%;margin:auto;height:auto;margin-top:10px;margin-bottom:30px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0"  style="font-size:16px;" >
 <tr>
      <td  bgcolor="#dfdfdf" height="1"></td>
    </tr>
  <tr > 
    <td height="45" bgcolor="#ffffff" ><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr  >
        <td height="45" style="border-bottom:1px solid #dcdfd8;padding-left:5px;">可用余额</td>
        <td height="45" align="right" style="border-bottom:1px solid #dcdfd8;font-size:16px; padding-right:10px;"><span style="color:#d79439;"><s:property value="bindNumInfo.balance" /> </span>元</td>
      </tr>
      <tr  >
        <td height="45" style="border-bottom:1px solid #dcdfd8;padding-left:5px;">实时话费</td>
        <td height="45" align="right" style="border-bottom:1px solid #dcdfd8;font-size:16px; padding-right:10px;"><span style="color:#d79439;"><s:property value="bindNumInfo.realtimecharge" /></span> 元</td>
      </tr>
      <tr  >
        <td height="45" style="border-bottom:1px solid #dcdfd8;padding-left:5px;">欠费金额</td>
        <td height="45" align="right" style="border-bottom:1px solid #dcdfd8;font-size:16px;padding-right:10px;"><span style="color:#d79439;"><s:property value="bindNumInfo.arrearage" /></span> 元</td>
      </tr>

    </table></td>
  </tr>

</table>

</div>
<div style="width:100%;margin:auto;height:auto;">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
  
    <tr>
    
      <td height="45" style="cursor:hand; " bgcolor="#FFFFFF" >
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" onclick="location.href='nowMonthBill.action?bindId=<s:property value="bindId"/>'"  style="font-size:16px;">    <tr>
      <td  bgcolor="#dcdfd8" height="1"></td>
    </tr>
        <tr>
       <td height="45" style="padding-left:5px;"><span style="float:left;width:auto;line-height:38px;">实时帐单</span><span style="float:right;width:30px;height:39px;line-height:39px;"><img src="images/btn_next3.png" width="26" height="39" /></span></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td  bgcolor="#dcdfd8" height="1"></td>
    </tr>
  </table>
</div>
<div style="width:100%;margin:auto;height:auto;"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  onclick="location.href='lastMonthBill.action?bindId=<s:property value="bindId"/>'"   style="font-size:16px;">
        <tr>
     <td height="45" bgcolor="#FFFFFF" style=" padding-left:5px;"><span style="float:left;width:auto;line-height:38px;">历史帐单</span><span style="float:right;width:30px;height:39px;line-height:39px;"><img src="images/btn_next3.png" width="26" height="39" /></span></td>
    </tr>
         <tr>
      <td  bgcolor="#dcdfd8" height="1"></td>
    </tr>
      </table></div>
<div id="envon" style="width:100%; background-color:#FFFFFF; border:1px solid #ccc; padding:10px; overflow:hidden; display:none;"> 
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';">
      <td align="center"  onclick="location.href='bindNumManager.action'" >绑定号码维护</td>
    </tr>
    <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center"  onclick="location.href='changeBindNum.action'">切换号码</td>
      
    </tr>
     <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center"  onclick="EV_closeAlert()">返 回</td>
    </tr>
  </table>
</div>
</body>
</html>
