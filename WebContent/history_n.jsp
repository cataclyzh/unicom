﻿<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript">
//用来记录要显示的DIV的ID值   
var EV_MsgBox_ID=""; //重要     
//弹出对话窗口(msgID-要显示的div的id)   
function EV_modeAlert(msgID){  
    //创建大大的背景框    
    var bgObj=document.createElement("div");  
    bgObj.setAttribute('id','EV_bgModeAlertDiv');  
    document.body.appendChild(bgObj);  
    //背景框满窗口显示   
    EV_Show_bgDiv();  
   //把要显示的div居中显示   
    EV_MsgBox_ID=msgID;  
    EV_Show_msgDiv();  
}  
  
//关闭对话窗口   
function EV_closeAlert(){  
    var msgObj=document.getElementById(EV_MsgBox_ID);  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    msgObj.style.display="none";  
    document.body.removeChild(bgObj);  
    EV_MsgBox_ID="";  
}  
  
//窗口大小改变时更正显示大小和位置   
window.onresize=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//窗口滚动条拖动时更正显示大小和位置   
window.onscroll=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//把要显示的div居中显示   
function EV_Show_msgDiv(){  
    var msgObj   = document.getElementById(EV_MsgBox_ID);  
   msgObj.style.display  = "block";  
    var msgWidth = msgObj.scrollWidth;  
    var msgHeight= msgObj.scrollHeight;  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var msgTop=bgTop+Math.round((bgHeight-msgHeight));  
    var msgLeft=bgLeft+Math.round((bgWidth-msgWidth)/2);  
    msgObj.style.position = "absolute";  
    msgObj.style.top      = msgTop+"px";  
    msgObj.style.left     = msgLeft+"px";  
    msgObj.style.zIndex   = "10001";  
      
}  
//背景框满窗口显示   
function EV_Show_bgDiv(){  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    bgObj.style.position   = "absolute";  
    bgObj.style.top        = bgTop+"px";  
   bgObj.style.left       = bgLeft+"px";  
    bgObj.style.width      = bgWidth + "px";  
    bgObj.style.height     = bgHeight + "px";  
    bgObj.style.zIndex     = "10000";  
    bgObj.style.background = "#777";  
    bgObj.style.filter     = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=60,finishOpacity=60);";  
    bgObj.style.opacity    = "0.6";  
}  
//网页被卷去的上高度   
function EV_myScrollTop(){  
    var n=window.pageYOffset   
    || document.documentElement.scrollTop   
    || document.body.scrollTop || 0;  
   return n;  
}  
//网页被卷去的左宽度   
function EV_myScrollLeft(){  
    var n=window.pageXOffset   
    || document.documentElement.scrollLeft   
    || document.body.scrollLeft || 0;  
    return n;  
}  
//网页可见区域宽   
function EV_myClientWidth(){  
    var n=document.documentElement.clientWidth   
    || document.body.clientWidth || 0;  
    return n;  
}  
//网页可见区域高   
function EV_myClientHeight(){  
    var n=document.documentElement.clientHeight   
    || document.body.clientHeight || 0;  
    return n;  
}  
</script>  

</head>
<body></div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="50" bgcolor="#fffffd" style="height:50px;line-height:50px;text-align:center;font-size:18px; font-weight:300 ">绑定号码:<s:property value="telnum" />
</td>
  </tr>
   <tr>
      <td  bgcolor="#d9dad5" height="1"></td>
    </tr>
</table>
<div style="width:100%;margin:auto;">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td style="margin-top:8px;" align="center">
  <div >
  
     <s:set name="mon" value="month" />
     <s:iterator id="ul" value="#session.monthList" status="em">
  	<s:if test="#mon==#ul.month">
  		<a class="on" data-tab="0" href="#">&nbsp;&nbsp;<s:property value="#ul.month"/>月</a>
  	</s:if>
  	<s:else>
  		<a data-tab="0" href="lastMonthBill.action?year=<s:property value="#ul.year"/>&month=<s:property value="#ul.month"/>&bindId=<s:property value="bindId"/>">&nbsp;&nbsp;<s:property value="#ul.month"/>月</a>
  	</s:else>    
  </s:iterator>
    </div>
 <div class=clear></div> 
<div style="height:6px;"></div>
  <div class=dy>该帐户<s:property value="year"/>年<s:property value="month"/>月话费信息如下：</div></td>
  </tr>
  </table>
</div>
<div style="height:6px;"></div>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <s:iterator id="ul" value="billInfoList" status="em">
 <s:if test="#ul.billlevel==1">
 <tr class="dy2">
 </s:if>
 <s:else>
  <tr class="dy3">
 </s:else>
   <td align="left" style="border-bottom:1px solid #c8c7cc;text-indent:30px;"><s:property value="#ul.billproject"/></td>
   <td align="right" style="border-bottom:1px solid #c8c7cc;padding-right:10px;"><span style="color:#d79439;"><s:property value="#ul.billcost"/></span>元</td>
 </tr>
 </s:iterator>
</table><div style="width:100%;height:50px;margin-right:auto;background:#f3913a;margin-left:auto;text-align:center;line-height:50px;font-size:22px;color:#fff;" onclick="location.href='nowMonthBill.action?bindId=<s:property value="bindId"/>'">实时话费</div>
<div id="envon" style="width:100%; background-color:#FFFFFF; border:1px solid #ccc; padding:10px; overflow:hidden; display:none;"> 
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';">
      <td align="center" class="fz12"  onclick="location.href='bindNumManager.action'" >绑定号码维护</td>
    </tr>
    <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center" class="fz12" onclick="location.href='changeBindNum.action'">切换号码</td>
      
    </tr>
     <tr><td style="background:#CCC;height:1px;" ></td></tr>
    <tr onmouseover="this.style.backgroundColor='#ebebeb';" onmouseout="this.style.backgroundColor='#fff';" >
      <td align="center" class="fz12" onclick="EV_closeAlert()">返 回</td>
    </tr>
  </table>
</div>
</body>
</html>
