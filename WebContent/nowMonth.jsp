<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />
<script language="JavaScript">
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}
</script>

<script>
function $(element){
return element = document.getElementById(element);
}
function $D(element){
var d=$(element);
var h=d.offsetHeight;
var maxh=300;
function dmove(){
if(h>=maxh){
d.style.height='90px';
clearInterval(iIntervalId);
}else{
h+=50; //设置层展开的速度
d.style.display='block';
d.style.height=h+'px';
}
}
iIntervalId=setInterval(dmove,2);
}
function $D2(element){
var d=$(element);
var h=d.offsetHeight;
var maxh=300;
function dmove(){
if(h<=0){
d.style.display='none';
clearInterval(iIntervalId);
}else{
h-=50;//设置层收缩的速度
d.style.height=h+'px';
}
}
iIntervalId=setInterval(dmove,2);
}
function $use(targetid,objN){
var d=$(targetid);
var sb=$(objN);
if (d.style.display=="block"){
    $D2(targetid);
       d.style.display="none";
       sb.innerHTML="<a href='#'/><IMG src='images/apptb3.png'/></a/>";
  } else {
    var p=document.getElementsByTagName("p");
    var span=document.getElementsByTagName("span");
 
    for(var i=0,l=p.length;i<l;i++){
		if(p[i]!=d){
    			 p[i].style.height=0;
      			 p[i].style.display="none";
       			span[i].innerHTML="展开";	
		}
    }
    $D(targetid);
       d.style.display="block";
       sb.innerHTML="<a href='#'/><IMG src='images/apptb2.png'/></a/>";
   }
}
</script>
<script language="JavaScript" type="text/JavaScript">
<!--
function toggle(targetid){
    if (document.getElementByIdx){
        target=document.getElementByIdx(targetid);
            if (target.style.display=="block"){
                target.style.display="none";
            } else {
                target.style.display="block";
            }
    }
}
</script>
</head>
<body>
<div class=apptop>
<div class=app>
<div class="l1"><img src="images/appreturn.png" width="19" height="33" /></div>
<div class="l2">实时话费</div>
<div class="r1" onMouseOver="MM_showHideLayers('menu1','','show')" onMouseOut="MM_showHideLayers('menu1','','hide')">
 <img src="images/appmore.png" width="34"   align="absmiddle" id=i  onmouseover="show(this)" onmouseout="hide()">
 
</div>
<div class=i_n_l id="menu1"  onMouseOver="MM_showHideLayers('menu1','','show')" onMouseOut="MM_showHideLayers('menu1','','hide')"  style="visibility: hidden"">
  <li><a href="changeBindNum.action"><img src="images/kjicon1.png" width="21" height="28" align="absmiddle" />&nbsp;&nbsp;切换号码</a></li>
  <li><a href="bindNumManager.action"><img src="images/kjicon2.png" width="24" height="25" align="absmiddle" />&nbsp;&nbsp;号码维护</a></li>
  </div>
</div></div>
<div class="index_slider">
<div class="tb">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="project_tl">
    <tr>
      <td width="94" align="center" ><a href="index.action"><img src="images/apppeople.png" width="73" height="73" border="0"/></a></td>
      <td class="tbtitle"><s:property value="telnum" /></td>
      <td align="center" valign="middle"     style="cursor:hand"><span id="stateBut" onClick="$use('class1content','stateBut')"><a href="#"><img src="images/apptb3.png" width="32" height="16"  style="cursor:hand "/></a></span></td>
    </tr>
  </table>
</div>
  <div class="tb2"  id="class1content">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center"><s:property value="#session.balance" />元</td>
      <td align="center"><s:property value="#session.realtimecharge" />元</td>
      <td align="center"><s:property value="#session.arrearage" />元</td>
    </tr>
    <tr>
      <td align="center">帐户余额</td>
      <td align="center">实时话费</td>
      <td align="center">欠费金额 </td>
    </tr>
  </table>
</div>
<div></div>
</div>
<div class=h6></div>
<div class=dy>您的当月话费信息如下：</div>
<div style="width:97%;margin:auto;">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="dy2">
        <td align="left" width="150" >增值业务费</td>
        <td align="center"><s:property value="billInfo.addedservice" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--代收费</td>
        <td align="center"><s:property value="billInfo.generationcharge" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--短消息费</td>
        <td align="center"><s:property value="billInfo.message" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--手机邮箱包月费</td>
        <td align="center"><s:property value="billInfo.mobilemail" /></td>
      </tr>
      <tr class="dy2">
        <td align="left">月固定费</td>
        <td align="center"><s:property value="billInfo.monthlyfixed" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--GPRSWAP月租费</td>
        <td align="center"><s:property value="billInfo.gprswap" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--来电显示费</td>
        <td align="center"><s:property value="billInfo.callerdisplay" /></td>
      </tr>
      <tr class="dy3">
        <td align="left">--3G包月费</td>
        <td align="center"><s:property value="billInfo.threegmonth" /></td>
      </tr>
      <tr class="dy2">
        <td align="left">消费合计</td>
        <td align="center"><s:property value="billInfo.totalconsumption" /></td>
      </tr>
      <tr class="dy2">
        <td align="left">抵扣合计</td>
        <td align="center"><s:property value="billInfo.totaldeduction" /></td>
      </tr>
      <tr class="dy2">
        <td align="left">实际应缴合计</td>
        <td align="center"><s:property value="billInfo.totalpay" /></td>
      </tr>

    </table></td>
  </tr>
  </table>
</div>
<div class=btu><img src="images/hisbutton.png" width="252" height="70" onclick="location.href='lastMonthBill.action'"/></div>
</body>
</html>
