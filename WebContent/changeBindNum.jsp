<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>手机帐单查询</title>
<link href="images/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.5.1.min.js"></script>
<script language="JavaScript" type="text/javascript">  

//用来记录要显示的DIV的ID值   
var EV_MsgBox_ID=""; //重要     
//弹出对话窗口(msgID-要显示的div的id)   
function EV_modeAlert(msgID){
  
    //创建大大的背景框   
    var bgObj=document.createElement("div");  
    bgObj.setAttribute('id','EV_bgModeAlertDiv');  
    document.body.appendChild(bgObj);  
    //背景框满窗口显示   
    EV_Show_bgDiv();  
   //把要显示的div居中显示   
    EV_MsgBox_ID=msgID;  
    EV_Show_msgDiv();  
   	$('#bindNum').focus();
}  
  
//关闭对话窗口   
function EV_closeAlert(){  
    var msgObj=document.getElementById(EV_MsgBox_ID);  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    msgObj.style.display="none";  
    document.body.removeChild(bgObj);  
    EV_MsgBox_ID=""; 
    $('body,html').animate({scrollTop:0},1000); 
}  
  
//窗口大小改变时更正显示大小和位置   
window.onresize=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//窗口滚动条拖动时更正显示大小和位置   
window.onscroll=function(){  
    if (EV_MsgBox_ID.length>0){  
        EV_Show_bgDiv();  
        EV_Show_msgDiv();  
    }  
}  
  
//把要显示的div居中显示   
function EV_Show_msgDiv(){  
    var msgObj   = document.getElementById(EV_MsgBox_ID);  
   msgObj.style.display  = "block";  
    var msgWidth = msgObj.scrollWidth;  
    var msgHeight= msgObj.scrollHeight;  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var msgTop=bgTop+Math.round((bgHeight-msgHeight)/2);  
    var msgLeft=bgLeft+Math.round((bgWidth-msgWidth)/2);  
    msgObj.style.position = "absolute";  
    msgObj.style.top      = msgTop+"px";  
    msgObj.style.left     = msgLeft+"px";  
    msgObj.style.zIndex   = "10001";  
      
}  
//背景框满窗口显示   
function EV_Show_bgDiv(){  
    var bgObj=document.getElementById("EV_bgModeAlertDiv");  
    var bgWidth=EV_myClientWidth();  
    var bgHeight=EV_myClientHeight();  
    var bgTop=EV_myScrollTop();  
    var bgLeft=EV_myScrollLeft();  
    bgObj.style.position   = "absolute";  
    bgObj.style.top        = bgTop+"px";  
   bgObj.style.left       = bgLeft+"px";  
    bgObj.style.width      = bgWidth + "px";  
    bgObj.style.height     = bgHeight + "px";  
    bgObj.style.zIndex     = "10000";  
    bgObj.style.background = "#777";  
    bgObj.style.filter     = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=60,finishOpacity=60);";  
    bgObj.style.opacity    = "0.6";  
}  
//网页被卷去的上高度   
function EV_myScrollTop(){  
    var n=window.pageYOffset   
    || document.documentElement.scrollTop   
    || document.body.scrollTop || 0;  
   return n;  
}  
//网页被卷去的左宽度   
function EV_myScrollLeft(){  
    var n=window.pageXOffset   
    || document.documentElement.scrollLeft   
    || document.body.scrollLeft || 0;  
    return n;  
}  
//网页可见区域宽   
function EV_myClientWidth(){  
    var n=document.documentElement.clientWidth   
    || document.body.clientWidth || 0;  
    return n;  
}  
//网页可见区域高   
function EV_myClientHeight(){  
    var n=document.documentElement.clientHeight   
    || document.body.clientHeight || 0;  
    return n;  
}  
</script>  
<script type="text/javascript"> 
function showDetail() { //在远方
//背景
  var bgObj=document.getElementById("bgDiv");
  bgObj.style.width = document.body.offsetWidth + "px";
  bgObj.style.height = screen.height + "px";

//定义窗口
  var msgObj=document.getElementById("msgDiv");
  msgObj.style.marginTop = -75 +  document.documentElement.scrollTop + "px";

//关闭
  document.getElementById("msgShut").onclick = function(){
  bgObj.style.display = msgObj.style.display = "none";
  }
  msgObj.style.display = bgObj.style.display = "block";
  msgDetail.innerHTML="<p align=center>小窗口里的内容</p><p align=center><A href=http://www.jscode.cn><FONT color=#0000ff>网页特效观止</FONT></A></p>"
}
</script>
<script type="text/javascript">
	
	function show(obj){
		var t = obj;
		t.style.width=document.body.clientWidth;
		t.style.height=document.body.clientHeight;
		window.onresize=function(){
			t.style.width=document.body.clientWidth;
			t.style.height=document.body.clientHeight;
		}
		t.style.display="";
	}
	function cl(id){
		$(id).style.display="none";
	}
	
</script>
<script language="JavaScript">

function MM_findObj(n, d) { //v4.0
  var p,i,x; 
   if(!d) d=document; 
   if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);
    }
  if(!(x=d[n])&&d.all) 
  	x=d.all[n]; 
  for (i=0;!x&&i<d.forms.length;i++) 
  	x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) 
  	x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById)
  	x=document.getElementById(n);
  	 return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  	if ((obj=MM_findObj(args[i]))!=null) {
  		 v=args[i+2];
    	if (obj.style) {
    	 obj=obj.style;
    	  v=(v=='show')?'visible':(v='hide')?'hidden':v;
        }
        obj.visibility=v; 
   }
}


</script>

<script language="JavaScript" type="text/JavaScript">

function toggle(targetid){
    if (document.getElementByIdx){
        target=document.getElementByIdx(targetid);
            if (target.style.display=="block"){
                target.style.display="none";
            } else {
                target.style.display="block";
            }
    }
}

function ChangeBindNum(id){	
			
			var datajson = {"bindId":id};
			var url = 'ajaxChangeBindNum.action';
			
			$.ajax({
	            type: "POST",
	            url: url,
	            dataType: "json",
	            data: datajson,
	            success: responseCreate,
	            error: function () {
	                alert("切换失败！");
	            }
	        });
		}
function responseCreate(data, textStatus, jqXHR){			
	if(data.status=="ok"){							
				location.href="index.action";
		}
		else {
			alert("切换号码失败！");
		}
}
</script>
</head>
<s:if test="isDefault==1">
<body onload="EV_modeAlert('envon'); $('#bindNum').focus();">
</s:if>
<s:else>
<body>
</s:else>
<div class=apptop>
<div class=app>
<div class="l1"><img src="images/appreturn.png" width="19" height="33" /></div>
<div class="l2">号码维护</div>
<div class="r1" onMouseOver="MM_showHideLayers('menu1','','show')" onMouseOut="MM_showHideLayers('menu1','','hide')">
 <img src="images/appmore.png" width="34"   align="absmiddle" id=i  onmouseover="show(this)" onmouseout="hide()"> 
</div>
<div class=i_n_l id="menu1"  onMouseOver="MM_showHideLayers('menu1','','show')" onMouseOut="MM_showHideLayers('menu1','','hide')"  style="visibility: hidden"">
  <li><a href="#"><img src="images/kjicon1.png" width="21" height="28" align="absmiddle" />&nbsp;&nbsp;切换号码</a></li>
  <li><a  href="javascript:"  onclick="EV_modeAlert('envon')"><img src="images/kjicon3.png" width="24" height="25" align="absmiddle" />&nbsp;&nbsp;新增号码</a></li>
  </div>
</div></div>
<div class="h6">
  
<div></div>
</div>
<s:iterator id="ul" value="bindNumList" status="em">
<div class="boxMore2"><li>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td onclick="ChangeBindNum(<s:property value="#ul.id" />)">
      <s:if test="#em.odd">
			<img src="images/phone1.png" width="72" height="72" align="absmiddle" />
		</s:if>
		<s:else>
			<img src="images/phone2.png" width="67" height="67" align="absmiddle" />
		</s:else>
       &nbsp;&nbsp;<s:property value="#ul.bindnumber" /></td>
      <td class="blue"></td>
      <td class="ora"></td>
    </tr>
</table>
</li>
</div>
</s:iterator>
<div id="envon" style=" width:300px; background-color:#FFFFFF; border:1px solid #ccc; padding:10px; overflow:hidden; display:none;"> 
 <table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="40" align="right" class="f16" width="120">新增号码：</td>
    <td align="left"><input type="text" name="bindNum" id="bindNum"  placeholder="输入联通电话号码" autofocus="autofocus" /></td>
  </tr>
  <tr>
    <td height="40" align="right" class="f16" width="120">设为默认：</td>
    <td align="left">
    <s:if test="isDefault==1">
    	<input type="checkbox" name="isDefault" id="isDefault" checked="true" disabled="true"/>
    </s:if>
    <s:else>
    	<input type="checkbox" name="isDefault" id="isDefault"  />
    </s:else>
    </td>
  </tr>
  <tr>
    <td height="90" colspan="2" align="left">
    <div style="margin:auto;text-align:center;">
    <input type="image" name="imageField" id="imageField" src="images/add1.png" onclick="bindNum()" style="cursor: pointer"/>     　
     <input type="image" name="imageField2" id="imageField2" src="images/can.png" onclick="EV_closeAlert()"  style="cursor: pointer"/></div></td>
    </tr>
</table>
</div>
</body>
</html>

